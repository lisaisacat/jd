--1080x1920 分辨率京东 App
--去掉了领取累计奖励
--去掉了进入购买任务后缩小范围
done = "去完成.jpg"
go = "jd去完成.png"
list = "做任务集爆竹.png"
task = "去领取.jpg"

x1=740
y1=723
x2=982
y2=1517

function isJD()
    pkg = app.frontPackageName()
    bid = "com.jingdong.app.mall"
    if pkg ~= bid then
        toast("返回京东 App") 
        app.runApp(bid)
        mSleep(3000)
        wid= widget.find({["text"]="累计任务奖励"})
        if wid == nil then
            keycode.back()
            toast("返回") 
            mSleep(5000) 
        end
    else
        toast("当前位置：京东 App") 
        mSleep(3000)
    end
end

function skim()--坐标需要更新
    wid= widget.find({["text"]="当前页点击浏览4个商品领爆竹"})--当前在加购商品界面
    if  wid  then
        event.tap(415,1137)--1
        toast("浏览第 1 个商品") 
        mSleep(3000)
        keycode.back()
        toast("返回") 
        mSleep(3000)
        event.tap(938,1142)--2
        toast("浏览第 2 个商品") 
        mSleep(3000)
        keycode.back()
        toast("返回") 
        mSleep(3000)
        event.tap(418,1911)--3
        toast("浏览第 3 个商品") 
        mSleep(3000)
        keycode.back()
        toast("返回") 
        mSleep(3000)
        event.tap(940,1912)--4
        toast("浏览第 4 个商品") 
        mSleep(3000)
        keycode.back()
        toast("返回") 
        mSleep(3000)
    end
end

function cart()--坐标需要更新
    wid= widget.find({["text"]="当前页浏览加购4个商品领爆竹"})--当前在加购商品界面
    if  wid  then
        event.tap(415,1137)--1
        toast("加购第 1 个商品") 
        mSleep(3000)
        keycode.back()
        toast("返回") 
        mSleep(3000)
        event.tap(938,1142)--2
        toast("加购第 2 个商品") 
        mSleep(3000)
        keycode.back()
        toast("返回") 
        mSleep(3000)
        event.tap(418,1911)--3
        toast("加购第 3 个商品") 
        mSleep(3000)
        keycode.back()
        toast("返回") 
        mSleep(3000)
        event.tap(940,1912)--4
        toast("加购第 4 个商品") 
        mSleep(3000)
        keycode.back()
        toast("返回") 
        mSleep(3000)
    end
end

function taskaward()
    x,y = image.findImageInRegion(task,x1,y1,x2,y2,500000)--任务列表去领取
    if x~=-1 and x~=-1 then
        event.tap(x,y)   
        toast("领取任务奖励~") 
        mSleep(3000)
    end
end

function backtolist()
    wid= widget.find({["text"]="累计任务奖励"})
    if wid == nil then
        keycode.back()
        toast("返回") 
        mSleep(5000) 
    end 
end

while true do
    isJD()
    x,y = image.findImageInRegion(list,800,1400,1000,1800,500000)--做任务集爆竹
    if x~=-1 and x~=-1 then
        event.tap(x,y)
        toast("做任务集爆竹") 
        mSleep(3000)
    end
 
    wid= widget.find({["text"]="累计任务奖励"})
    if  wid  then
        toast("城城分现金和跳转小程序任务需提前手动完成噢")
        mSleep(3000)
        taskaward()
        toast("查找【去完成】按钮范围纵坐标："..y2)
        mSleep(3000)
        x,y = image.findImageInRegion(done,x1,y1,x2,y2,500000)--去完成任务
        if x~=-1 and x~=-1 then
            event.tap(x,y)
            toast("去完成然后等待 12 秒") 
            mSleep(12*1000)
            skim()
            cart()
            wid= widget.find({["text"]="累计任务奖励"})
            if wid == nil then
                keycode.back()
                toast("返回") 
                mSleep(5000) 
                backtolist()
            end
        end
        x,y = image.findImageInRegion(go,x1,y1,x2,y2,500000)--去完成任务
        if x~=-1 and x~=-1 then
            event.tap(x,y)
            toast("去完成然后等待 12 秒") 
            mSleep(12*1000)
            skim()
            cart()
            wid= widget.find({["text"]="累计任务奖励"})
            if wid == nil then
                keycode.back()
                toast("返回") 
                mSleep(5000) 
                backtolist()
            end
        end
    end
end