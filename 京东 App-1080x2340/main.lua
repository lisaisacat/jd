--京东 1080x2340
done = "去完成.jpg"
go = "jd去完成.png"
list = "做任务集爆竹.jpg"
award = "去领取累计奖励.jpg"
task = "去领取.jpg"
shopping = "逛店铺.jpg"

regiony0=1100
regiony=2050

function isJD()
    pkg = app.frontPackageName()
    bid = "com.jingdong.app.mall"
    if pkg ~= bid then
        --keycode.recent("京东")
        toast("返回京东 App") 
        app.runApp(bid)
        mSleep(3000)
        wid= widget.find({["text"]="累计任务奖励"})
        if wid == nil then
            keycode.back()
            toast("返回") 
            mSleep(5000) 
        end
    else
        toast("当前位置：京东 App") 
        mSleep(3000)
    end
end

function skim()
    wid= widget.find({["text"]="当前页点击浏览4个商品领爆竹"})--当前在加购商品界面
    if  wid  then
        event.tap(415,1137)--1
        toast("浏览第 1 个商品") 
        mSleep(3000)
        keycode.back()
        toast("返回") 
        mSleep(3000)
        event.tap(938,1142)--2
        toast("浏览第 2 个商品") 
        mSleep(3000)
        keycode.back()
        toast("返回") 
        mSleep(3000)
        event.tap(418,1911)--3
        toast("浏览第 3 个商品") 
        mSleep(3000)
        keycode.back()
        toast("返回") 
        mSleep(3000)
        event.tap(940,1912)--4
        toast("浏览第 4 个商品") 
        mSleep(3000)
        keycode.back()
        toast("返回") 
        mSleep(3000)
    end
end

function cart()
    wid= widget.find({["text"]="当前页浏览加购4个商品领爆竹"})--当前在加购商品界面
    if  wid  then
        event.tap(415,1137)--1
        toast("加购第 1 个商品") 
        mSleep(3000)
        keycode.back()
        toast("返回") 
        mSleep(3000)
        event.tap(938,1142)--2
        toast("加购第 2 个商品") 
        mSleep(3000)
        keycode.back()
        toast("返回") 
        mSleep(3000)
        event.tap(418,1911)--3
        toast("加购第 3 个商品") 
        mSleep(3000)
        keycode.back()
        toast("返回") 
        mSleep(3000)
        event.tap(940,1912)--4
        toast("加购第 4 个商品") 
        mSleep(3000)
        keycode.back()
        toast("返回") 
        mSleep(3000)
    end
end

function buy()
    wid= widget.find({["text"]="好货特卖"})--当前在加购商品界面
    if  wid  then
        regiony = y - 100
        keycode.back()
        toast("不买东西，缩小查找范围："..regiony) 
        mSleep(3000)
        if regiony <= 1500 then
            regiony = 1500
        end
    end
end

function getaward()
    x,y = image.findImageInRegion(award,433,832,997,985,500000)--领累计任务奖励
    if x~=-1 and x~=-1 then
        event.tap(x+50,y+15)
        toast("领取累计次数奖励~") 
        mSleep(2000)
        event.tap(988,676)
        mSleep(2000)
    end
end

function taskaward()
    x,y = image.findImageInRegion(task,720,regiony0,984,regiony,500000)--任务列表去领取
    if x~=-1 and x~=-1 then
        event.tap(x,y)   
        toast("领取任务奖励~") 
        mSleep(3000)
    end
end

function shop()
    x,y = image.findImageInRegion(shopping,730,378,982,503,500000)--任务列表去领取
    if x~=-1 and x~=-1 then
        event.tap(x,y)   
        toast("逛店铺~") 
        mSleep(3000)
        keycode.back()
        mSleep(3000)
    end
end

function backtolist()
    wid= widget.find({["text"]="累计任务奖励"})
    if wid == nil then
        keycode.back()
        toast("返回") 
        mSleep(5000) 
    end 
end

while true do
    isJD()
    x,y = image.findImageInRegion(list,816,763,1071,1632,500000)--做任务集爆竹
    if x~=-1 and x~=-1 then
        event.tap(x,y)
        toast("做任务集爆竹") 
        mSleep(3000)
    end
   
    wid= widget.find({["text"]="累计任务奖励"})
    if  wid  then
        toast("城城分现金任务需手动完成噢")
        mSleep(3000)
        getaward()
        taskaward()
        x,y = image.findImageInRegion(done,720,regiony0,984,regiony,500000)--去完成任务
        if x~=-1 and x~=-1 then
            event.tap(x,y)
            toast("去完成然后等待 12 秒") 
            mSleep(12*1000)
            buy()
            skim()
            cart()
            wid= widget.find({["text"]="累计任务奖励"})
            if wid == nil then
                keycode.back()
                toast("返回") 
                mSleep(5000) 
                backtolist()
            end
        end
        x,y = image.findImageInRegion(go,720,regiony0,984,regiony,500000)--去完成任务
        if x~=-1 and x~=-1 then
            event.tap(x,y)
            toast("去完成然后等待 12 秒") 
            mSleep(12*1000)
            buy()
            skim()
            cart()
            shop()
            wid= widget.find({["text"]="累计任务奖励"})
            if wid == nil then
                keycode.back()
                toast("返回") 
                mSleep(5000) 
                backtolist()
            end
        end
    end
end